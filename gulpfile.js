const postcss = require('gulp-postcss');
const gulp = require('gulp');
const { series } = require('gulp');
const copy = require('gulp-copy');

const concat = require('gulp-concat');
const zip = require('gulp-zip');

const uglifycss = require('gulp-uglifycss');

const run = require('gulp-run');

const BUILD_DIR = 'build/'

const package = () => (
  gulp.src(`${BUILD_DIR}/**/*`)
      .pipe(zip('hillopurkki.zip'))
      .pipe(gulp.dest('dist'))
)

const css = () => (
  gulp.src('src/*.css')
    .pipe(postcss([
      require('postcss-import'),
      require('tailwindcss/nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(gulp.dest(BUILD_DIR))
)

const copyFiles = () => (
  gulp
    .src(['src/**/*.php', 'src/theme.json'])
    .pipe(copy(BUILD_DIR, { prefix: 1 }))
)

const copyAssets = () => (
  gulp
    .src(['assets/**/*.*'])
    .pipe(copy(BUILD_DIR, { prefix: 0 }))
)

const optimizeCss = () => {
  return gulp
    .src('./src/*.css')
    .pipe(postcss([
      require('postcss-import'),
      require('tailwindcss/nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(uglifycss({ "uglyComments": false}))
    .pipe(gulp.dest('build/'))
}

const copyDocker = () => {
  return run('./cp_docker.sh').exec();
}

exports.package = series(copyFiles, optimizeCss, copyAssets, package);
exports.css = css;
exports.optimize = optimizeCss;
exports.default = series(css, copyFiles, copyAssets, copyDocker)
