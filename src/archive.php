<?php
/**
 * @author Joonatan Kuosa
 * @date 2021-03-30
 *
 * Template for archives
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<?php
$title = !empty(get_the_archive_title()) ? get_the_archive_title() : 'Blog';
?>
<body>

  <div class="mx-0 flex justify-center mt-0 lg:mt-30">
  <div class="page-2xl mx-0 lg:mx-2 flex flex-col lg:flex-row flex-grow">

    <main id="main" class="archive flex-grow mx-2 lg:mx-0 lg:mr-3" tabindex="-1">
      <h1 class="w-full page-title bg-primary text-white"><?= $title; ?></h1>
      <div class="w-full flex md:mx-2 flex-wrap space-around">
      <?php
      if ( have_posts() ):
        while ( have_posts() ):
          the_post();
          ?>
          <div class="max-w-full w-full lg:w-1/2 xl:w-1/3 p-0 mb-3">
            <?php get_template_part( 'template-parts/card-element' ); ?>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
      </div>
      <nav class="w-full flex justify-between lg:mx-6 my-3">
        <div><?php previous_posts_link( '<div class="btn-sec-sm">' . __('Newer posts') . '</div>' ); ?></div>
        <div><?php next_posts_link( '<div class="btn-sec-sm">' . __('Older posts') . '</div>' ); ?></div>
      </nav>
      <?php if ( ! is_paged() && !empty(term_description()) ): ?>
      <div class="w-full py-2 my-2 mb-3 md:p-3 md:m-3 bg-blue-200">
        <div class="page-md mx-2 md:mx-auto">
          <?= wpautop( apply_filters( 'the_content', term_description() ) ); ?>
        </div>
      </div>
      <?php endif; ?>
    </main>
    <aside class="hidden md:block">
      <?php get_sidebar(); ?>
    </aside>

  </div>
  </div>

  <?php get_footer(); ?>
</body>

</html>
