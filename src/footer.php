<?php
/**
 * Footer for Shedim theme
 */
?>

<footer class="bg-primary">
  <div class="page-xl flex flex-col md:flex-row text-white mx-auto pt-6 pb-32 lg:pb-6 justify-between text-center">

  <div class="w-full md:w-1/3 flex justify-center md:justify-start">
    <?php if ( function_exists('cn_social_icon') ) echo cn_social_icon(); ?>
  </div>

  <div class="w-full md:w-1/3 flex flex-col justify-center">
    <?php if ( get_privacy_policy_url() ): ?>
      <div>
        <a href="<?= get_privacy_policy_url(); ?>"><?php _e('Tietosuojaseloste'); ?></a>
      </div>
    <?php endif; ?>
    <div>
      <?= esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
      &copy; <?= date_format(new DateTime(), 'Y'); ?>
    </div>
  </div>

  <div class="w-full md:w-1/3 flex flex-col justify-center md:justify-end text-center md:text-right">
    <?php if (!empty(get_theme_mod( 'shedim_email' ))): ?>
      <a class="mr-4" href="mailto:<?php echo get_theme_mod( 'shedim_email' ); ?>">
         <?php echo get_theme_mod( 'shedim_email' ); ?>
      </a>
    <?php endif; ?>
    <?php if (!empty(get_theme_mod( 'shedim_ytunnus' ))): ?>
      <div class="mr-3">
        Y-tunnus: <?php echo get_theme_mod( 'shedim_ytunnus' ); ?>
      </div>
    <?php endif; ?>
  </div>

  </div>
</footer>
<?php wp_footer(); ?>
