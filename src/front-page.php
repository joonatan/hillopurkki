<?php
/**
 * Homepage
 * Render only the content for front-page (discard title)
 * Full page-width with no margins at all allowing the use of cover images and
 * other user props from the front-end.
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>

  <div class="mx-0 flex justify-center bg-bg">
    <main id="main" class="front-page w-full relative" tabindex="-1">
      <div class="w-full relative">
        <div class="hero-section bg-primary relative z-10 w-full pt-0 flex justify-center">
          <div class="hero-container bg-none pt-6 w-full flex flex-col md:flex-row page-xl mx-auto mt-4 justify-around xl:justify-between">
            <div class="w-auto lg:w-2/3 mx-6 text-white uppercase">
              <h1 class="font-display text-small-title sm:text-mobile-title 2lg:text-page-title uppercase leading-none mb-3 font-semibold text-white">
                 <?= get_theme_mod( 'shedim_theme_heading' ); ?>
              </h1>
              <div class="page-md font-display text-mobile-subtitle sm:text-subtitle font-medium leading-none text-white mb-4 lg:-mb-6">
                <?= get_theme_mod( 'shedim_theme_subheading' ); ?>
              </div>
            </div>
            <div class="relative w-auto lg:w-1/3">
              <img width="280" height="513" class="hero-img hidden md:block absolute z-10 -mt-6 mr-6 xl:mr-40 w-auto" src="<?= get_template_directory_uri() . '/assets/hillo-phone.png'; ?>" />
            </div>
          </div>
        </div>
        <div class="lg:hidden bg-white py-6">
          <div class="flex justify-around bg-primary pt-4 -mb-4">
            <div class="mx-6 my-6">
              <a href="<?= get_theme_mod('shedim_mobile_cta_link'); ?>"
                   class="block font-btn font-semibold bg-cta text-white text-mobile-subtitle py-3 px-10 text-center uppercase rounded-md leading-none hover:brightness-75 focus:brightness-75">
                  <?= get_theme_mod('shedim_mobile_cta_text'); ?>
                </a>
            </div>
          </div>
        </div>
        <svg class="hero-svg relative w-full -mt-6 pt-0 lg:pt-10" viewBox="0 0 2560 220">
          <rect class="lg:hidden fill-white" x="620" y="10" width="1940" height="210" />
          <path id="Path_25" data-name="Path 25" class="fill-bg" d="M-8,100s475,155,850,50,1000-61,1920-61V-7H-8Z" transform="translate(0 20)" />
          <path id="Path_26" data-name="Path 26" class="fill-primary" d="M-8,100s475,155,840,40,1820-61,1820-61V-7H-8Z" />
        </svg>
      </div>
      <!-- White background should extend under the hero-section -->
      <div class="-mt-20 xl:-mt-40 -mb-3 pt-20 page-xl mx-auto bg-white">
      </div>
      <div class="page-xl mx-auto bg-white">
        <?php
        if ( have_posts() ) {
          while ( have_posts() ) {
            the_post();
            ?>
            <article class="entry-content pt-12 xl:pt-20 3xl:pt-15">
              <?php the_content(); ?>
            </article>
            <?php
          }
        }
        ?>

        <?php get_template_part( 'template-parts/email-list-section' ); ?>

        <?php echo shedim_get_edit_posts_btn( __( 'Edit' ) ); ?>
      </div>
    </main>

  </div>

  <?php get_footer(); ?>

</body>

</html>
