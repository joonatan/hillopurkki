<?php
/**
 * @author Joonatan Kuosa
 * @date 2020-10-03
 *
 * @desc Shedim Portfolio theme
 */

// allow shortcodes in text and html widgets
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );

// Remove the admin bar since it doesn't play nice with the full width page
// TODO could add it back if we can get the CSS working with it
add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );
// no hard-coded title tag
add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'custom-logo', array( 'width' => 360, 'height' => 92,'flex-width' => true ) );

add_theme_support( 'wp-block-styles' );

/* disable font sizes since they don't work without tailwind overrides */
add_theme_support( 'disable-custom-font-sizes' );

/* Preload Google Fonts */
function themeprefix_load_fonts() {
  $font = 'Teko:400,500,600,700|Robot+Condensed:400,700';
    ?>
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">
<link rel="preload" href="https://fonts.googleapis.com/css?family=<?= $font; ?>" as="fetch" crossorigin="anonymous">
<script type="text/javascript">
!function(e,n,t){"use strict";var o="https://fonts.googleapis.com/css?family=<?= $font; ?>",r="__3perf_googleFontsStylesheet";function c(e){(n.head||n.body).appendChild(e)}function a(){var e=n.createElement("link");e.href=o,e.rel="stylesheet",c(e)}function f(e){if(!n.getElementById(r)){var t=n.createElement("style");t.id=r,c(t)}n.getElementById(r).innerHTML=e}e.FontFace&&e.FontFace.prototype.hasOwnProperty("display")?(t[r]&&f(t[r]),fetch(o).then(function(e){return e.text()}).then(function(e){return e.replace(/@font-face {/g,"@font-face{font-display:swap;")}).then(function(e){return t[r]=e}).then(f).catch(a)):a()}(window,document,localStorage);
</script>
    <?php
}
add_action( 'wp_head', 'themeprefix_load_fonts' );

/* Load fonts on Editor side
 * TODO should preload
 **/
function prefix_block_styles() {
  wp_enqueue_style( 'prefix-editor-font', "https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Teko:wght@400;500;600;700&display=swap");
}

add_action( 'enqueue_block_editor_assets', 'prefix_block_styles' );

function shedim_scripts() {
  wp_enqueue_style( 'shedim_style', get_template_directory_uri() . '/style.css', [], time() );
  //wp_enqueue_script('shedim_js', get_template_directory_uri() . '/assets/js/shedim.js');
  wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', [], false, false );
}
/* Load the base CSS first; since we will override it with inline styles */
add_action( 'wp_enqueue_scripts', 'shedim_scripts', 1);

/* overrides to WP inline styles */
function shedim_style_overrides () {
  $custom_css = "
    .wp-block-columns {
      display: grid;
    }
    .wp-block-button__link:visited {
      color: var(--wp--preset--color--primary);
    }
    @media (max-width: 600px) {
      h2 {
        font-size: 40px;
      }
    }
    ";
  // FIXME wp_add_inline_style doesn't work for some reason
  //wp_add_inline_style( 'shedim_inline_override', $custom_css );
  echo '<style type="text/css" id="shedim-theme-inline-css">'
    . $custom_css
    . '</style>';
}
//add_action( 'wp_enqueue_scripts', 'shedim_style_overrides' );
add_action( 'wp_head', 'shedim_style_overrides' );

function register_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu' ),
      'shortcut-menu' => __( 'Mobile Shortcut Menu' ),
    )
  );
}
add_action( 'init', 'register_menus' );

function shedim_new_customizer_settings($wp_customize) {
  $wp_customize->add_setting('shedim_email', array(
    'sanitize_callback' => 'sanitize_email',
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_email',
    array(
      'label' => 'Contact email',
      'section' => 'title_tagline',
      'settings' => 'shedim_email',
      'type' => 'email'
    )
  ));

  $wp_customize->add_setting('shedim_theme_heading', array(
    'sanitize_callback' => 'sanitize_text_field'
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_theme_heading',
    array(
      'label' => 'Heading',
      'section' => 'title_tagline',
      'settings' => 'shedim_theme_heading',
      'type' => 'text'
    )
  ));

  $wp_customize->add_setting('shedim_theme_subheading', array(
    'sanitize_callback' => 'sanitize_text_field'
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_theme_subheading',
    array(
      'label' => 'Subheading',
      'section' => 'title_tagline',
      'settings' => 'shedim_theme_subheading',
      'type' => 'text'
    )
  ));

  /* Link for mobile CTA */
  $wp_customize->add_setting('shedim_mobile_cta_link', array(
    'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_mobile_cta_link',
    array(
      'label' => 'Mobile CTA link',
      'section' => 'title_tagline',
      'settings' => 'shedim_mobile_cta_link',
      'type' => 'text'
    )
  ));

  $wp_customize->add_setting('shedim_mobile_cta_text', array(
    'sanitize_callback' => 'sanitize_text_field'
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_mobile_cta_text',
    array(
      'label' => 'Mobile CTA text',
      'section' => 'title_tagline',
      'settings' => 'shedim_mobile_cta_text',
      'type' => 'text'
    )
  ));


  $wp_customize->add_setting('shedim_primary_colour', array(
    'default' => '#000000',
    'sanitize_callback' => 'sanitize_hex_color',
  ));
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'shedim_primary_colour',
    array(
      'label' => 'Primary Colour',
      'section' => 'title_tagline',
      'settings' => 'shedim_primary_colour'
    )
  ));

  $wp_customize->add_setting('shedim_has_hamburger', array(
    'default' => 'true',
    'sanitize_callback' => 'sanitize_text_field',
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_has_hamburger',
    array(
      'label' => 'Hamburger On',
      'section' => 'title_tagline',
      'settings' => 'shedim_has_hamburger',
      'type' => 'checkbox'
    )
  ));

  $wp_customize->add_setting('shedim_header_opacity', array(
    'default' => 'true',
    'sanitize_callback' => 'sanitize_text_field',
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_header_opacity',
    array(
      'label' => 'Header Opacity',
      'section' => 'title_tagline',
      'settings' => 'shedim_header_opacity',
      'type' => 'number'
    )
  ));

  $wp_customize->add_setting('shedim_cta_colour', array(
    'default' => '#000000',
    'sanitize_callback' => 'sanitize_hex_color',
  ));
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'shedim_cta_colour',
    array(
      'label' => 'CTA Colour',
      'section' => 'title_tagline',
      'settings' => 'shedim_cta_colour'
    )
  ));

  // ---------------------- Footer Section -----------------------------------
  $wp_customize->add_section('shedim_theme_footer', array(
    'title' => 'Footer',
    'description' => '',
    'priority' => 30,
  ));

  $wp_customize->add_setting('shedim_email_list_form_id', array(
    'sanitize_callback' => 'absint',
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_email_list_form_id',
    array(
      'label' => 'Email list form id',
      'section' => 'shedim_theme_footer',
      'settings' => 'shedim_email_list_form_id',
      'type' => 'number'
    )
  ));

  $wp_customize->add_setting('shedim_ytunnus', array(
    'sanitize_callback' => 'sanitize_text_field',
  ));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'shedim_ytunnus',
    array(
      'label' => 'Y tunnus',
      'section' => 'shedim_theme_footer',
      'settings' => 'shedim_ytunnus',
    )
  ));
}
add_action('customize_register', 'shedim_new_customizer_settings');

function shedim_css_output() {
  $col = get_theme_mod( 'shedim_primary_colour', null );
  $cta = get_theme_mod( 'shedim_cta_colour', null );
  $op = get_theme_mod( 'shedim_header_opacity', 1 );

  if ($col !== null) {
    (strlen($col) === 4) ? list($r, $g, $b) = sscanf($col, "#%1x%1x%1x") : list($r, $g, $b) = sscanf($col, "#%2x%2x%2x");

    // TODO this method of generating the CSS is so messy (media queries)
    echo '<style type="text/css" id="custom-theme-css">'
      . '.btn-cta {background:' . " $cta; }"
      . '.bg-cta {background:' . " $cta; }"
      . '.text-cta {color:' . " $cta; }"
      . '.bg-primary {background:' . " rgba($r, $g, $b, 1) ;} "
      . '.bg-header {background:' . " rgba($r, $g, $b, $op) ;} "
      . '@media only screen and (min-width: 950px) { .lg\:bg-header {background:' . " rgba($r, $g, $b, $op) ;} } "
      . '@media only screen and (min-width: 950px) { .lg\:bg-transparent {background:' . " rgba(0, 0, 0, 0) ;} } "
      . '.text-primary {color: ' . $col . ';} '
      . 'article p a {color: ' . $col . ';}'
      . '</style>';
  }
}
//add_action( 'wp_head', 'shedim_css_output');

function shedim_get_edit_posts_btn( $text ) {
  if (current_user_can( 'edit_posts' )) {
    return '<div class="section-inner bg-list-section py-4">'
      . '<div class="w-full text-center">'
      . '<a class="btn" href="' . get_edit_post_link() . '">' .$text .'</a>'
      . '</div>'
      . '</div>';
  } else {
    return '';
  }
}
