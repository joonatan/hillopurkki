<?php
/**
 * Header for Portfolio theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >

  <link rel="profile" href="https://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>
</head>

<?php

/* Navigation:
 * - Desktop is fixed to the top
 * - Mobile is fixed to the bottom
 * - Desktop has bg-opacity 0 for front-page so the cover image shines through
 * - Mobile has bg-opacity 1 everywhere
 * - Desktop has bg-opacity 1 for all other pages
 */
// Don't fix the desktop navigation for now
$nav_cls = 'lg:relative lg:bg-primary';
// TODO should try sticky desktop version without the logo
// (but that requires us to move the logo outside the nav structure)
// also we need to add background to the meny (half opacity or something)
// problem with cover images being dark, but the site being light otherwise
// so how to change create a nice looking background without doing hard lines
?>
<body>
  <header id="site-header" class="z-104 pb-0 font-nav text-xl relative">
      <a class="skip-main" href="#main"><?php _e('Skip to content') ?></a>
      <?php
      $hamb_cls = get_theme_mod( 'shedim_has_hamburger', false ) ? 'block' : 'hidden';
      $meny_cls = get_theme_mod( 'shedim_has_hamburger', false ) ? 'block lg:flex' : 'hidden lg:flex';
      $main_menu_cls = get_theme_mod( 'shedim_has_hamburger', false ) ? 'w-80p' : 'w-full';
      ?>
      <!-- mobile logo at the top of the page -->
      <div class="flex lg:hidden w-full bg-primary justify-center flex-shrink-0 pt-3 pb-6 -mb-3">
        <?php
        if ( function_exists( 'the_custom_logo' ) ) {
          the_custom_logo();
        }
        ?>
      </div>
      <nav class="w-full flex flex-wrap fixed <?= $nav_cls; ?> bottom-0 lg:bottom-auto lg:top-0 -mb-3 pb-3 lg:px-3 xl:px-12">
        <div class="bg-darkgray lg:hidden opacity-100 bottom-0 right-0 <?= $main_menu_cls; ?>">
          <?php
          // TODO how to configure the icons? CSS :before?
          wp_nav_menu( array(
            'menu' => 'shortcut-menu',
            'theme_location' => 'shortcut-menu',
            'depth' => 1,
            'container' => false,
            'menu_class' => 'shortcut-menu',
            'link_before' => '<span class="shortcut-icon font-ma-icons text-xl" aria-hidden="true"></span>'
          ))
          ?>
        </div>
        <input type="checkbox" id="menyAvPaa" class="<?= $hamb_cls; ?> lg:hidden p-4 ml-auto w-15p" role="button"
               onchange="hideOverflow(this);"
               onkeypress="checkOnEnter(this, event);"
               aria-label="<?php _e('menu', 'shedim'); ?>">
        </input>
        <div id="meny" class="w-full <?= $meny_cls; ?> bg-primary lg:bg-transparent text-white lg:text-nav-links lg:justify-between z-104 fixed lg:relative bottom-24 lg:bottom-0 right-0 items-center font-medium">
          <div class="flex w-auto items-center flex-shrink-0 lg:mr-4">
            <?php
            if ( function_exists( 'the_custom_logo' ) ) {
              the_custom_logo();
            }
            ?>
          </div>
          <?php
          wp_nav_menu( array(
            'menu' => 'primary-menu',
            'theme_location' => 'primary-menu',
            'depth' => 3,
            'container' => false,
            'menu_class' => 'flex flex-col lg:flex-row mr-2 ml-2 md:m-left-auto my-3',
            'after' => '<input type="checkbox" role="button" class="icon open-sub-menu" aria-label="expand"></input>'
          ))
          ?>
        </div>
      </nav>
  </header>
</body>

</html>
