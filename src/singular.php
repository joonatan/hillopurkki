<?php
/**
 * @author Joonatan Kuosa
 * @date 2020-10-03
 *
 * Template for pages
 * The base template is 1320px wide the content width is 1320px
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <!-- Empty divs with flex-grow create margins while allowing the main element to resize -->
  <div class="bg-bg flex justify-around">
  <div class="page-xl w-1320 mt-6 bg-white">
    <main id="main" class="page page-xl mx-0" tabindex="-1">
      <?php
      if ( have_posts() ) {
        while ( have_posts() ) {
          the_post();
          get_template_part( 'template-parts/content', get_post_type() );
        }
      }
      ?>
    </main>
    <?php echo shedim_get_edit_posts_btn( __( 'Edit' ) ); ?>
  </div>
  </div>

  <?php get_footer(); ?>
</body>

</html>
