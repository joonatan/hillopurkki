<?php
  $desc = get_the_excerpt();
?>

<!-- need zero margin and padding so that the percentage width works properly -->
<div class="flex flex-col mx-3 border-2 border-gray-400 h-full">
  <a class="flex-1 flex flex-col card-section" href="<?php echo esc_url( get_permalink() ); ?>">
    <?php // @todo need a custom thumbnail size or smaller cards for 300x300 (medium) ?>
    <?php the_post_thumbnail('medium_large') ?>
    <div class="card-text">
      <h3 class="text-opacity-100 text-gray-900 font-bold p-0 ml-3 mb-2">
         <?php the_title(); ?>
      </h3>
      <p class="text-gray-700 card-text-box"><?php echo $desc; ?></p>
    </div>
  </a>
  <div class="flex-none flex justify-center rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
    <?php
    foreach ( $project_types as $cat ) :
      $href = get_term_link($cat);
      echo '<a href="' . esc_url($href) . '" '
        . 'class="btn-sm my-3 mx-auto md:mx-3">' . $cat->name . '</a>';
    endforeach;
    ?>
  </div>
</div>
