<?php
/**
 */
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <header>
    <?php the_title( '<h1 class="text-subtitle md:text-4xl page-title mt-6 mb-0 pb-0 mx-6">', '</h1>' ); ?>
  </header>

  <div class="entry-content">
    <?php the_content(); ?>
  </div>

  <?php if (false && is_single()): ?>
    <?php get_template_part( 'template-parts/navigation' ); ?>
  <?php endif; ?>

</article><!-- .post -->
