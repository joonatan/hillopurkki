<div class="email-list-section pt-6">
  <div class="page-md mx-auto">
  <?php
    // TODO check that FrmFormsController is available
    // TODO header should be h2 not h3
    $form_id = get_theme_mod( 'shedim_email_list_form_id' );
    echo FrmFormsController::get_form_shortcode( array( 'id' => $form_id, 'title' => true, 'description' => true ) );
  ?>
  </div>
</div>

