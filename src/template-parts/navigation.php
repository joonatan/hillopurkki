<?php
/**
 * Displays the next and previous post navigation in single posts.
 */

$next_post = get_next_post();
$prev_post = get_previous_post();

if ( $next_post || $prev_post ): ?>

  <nav class="flex justify-between lg:mx-10 my-4" aria-label="<?php esc_attr_e( 'Post' ); ?>">
    <?php if ( $prev_post ): ?>
      <a class="previous-post btn-sec-sm" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
        <span class="arrow" aria-hidden="true">&larr;</span>
        <span class="title"><span class="title-inner"><?php echo wp_kses_post( get_the_title( $prev_post->ID ) ); ?></span></span>
      </a>
    <?php else: ?>
      <div></div>
    <?php endif; ?>

    <!-- branch based on post_type -->
    <?php
    $blog_ref = get_permalink( get_option( 'page_for_posts' ) );
    $projects_ref = get_post_type_archive_link( 'project' );
    $ptype =  get_post()->post_type;
    $ref = ($ptype === 'project') ? $projects_ref : $blog_ref;
    ?>
    <a class="btn-sec-sm text-center mx-10" href="<?php echo esc_url($ref); ?>">
      <span class="arrow" aria-hidden="true">&uarr;</span>
      <?php _e('Up') ?>
    </a>

    <?php if ( $next_post ): ?>
      <a class="next-post btn-sec-sm" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
        <span class="title"><span class="title-inner"><?php echo wp_kses_post( get_the_title( $next_post->ID ) ); ?></span></span>
        <span class="arrow" aria-hidden="true">&rarr;</span>
      </a>
    <?php else: ?>
      <div></div>
    <?php endif; ?>
  </nav><!-- .pagination-single -->

<?php endif;
