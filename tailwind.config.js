// tailwind.config.js
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  content: [
    './src/**/*.php'
  ],
  theme: {
    fontFamily: {
      display: ['Teko', 'Roboto', 'sans-serif'],
      nav: ['Teko', 'Roboto', 'sans-serif'],
      btn: ['Teko', 'Roboto', 'sans-serif'],
      body: ['Roboto Condensed', 'Roboto', 'sans-serif'],
      'ma-icons': ['Material Icons'],
    },
    extend: {
      // TODO fill should use values from colour
      fill: {
        'primary': '#157AC8',
        'bg': '#d5e5f1',
        'white': 'white',
      },
      colors: {
        // 60% the main theme colour (either black or white)
        //'primary': '#2493E8', original colour that is not accessible
        'dark': '#383838',
        'primary': '#157AC8',
        'bg': '#d5e5f1',
        'header': 'rgba(0, 0, 0, 0.2)',
        'nav-links': 'rgba(255, 255, 255, 1)',
        'cta': '#c94e00',
        'list-section': 'rgb(222, 235, 244)',
        'darkgray': '#434343',
        //'list-section': '#d5d3d3', // gray version
        // 30% the main off colour (usually brand colour)
        //'secondary': '#c3b9d7',
        // 10% CTA and other really important things
        //'accent': '#22bb11',
      },
      padding: {
        '40': '10rem',
      },
      margin: {
        '-60': '-15rem',
        '-40': '-10rem',
        '-20': '-5rem',
        '40': '10rem',
        '60': '15rem',
      },
      inset: {
        '24': '6rem',
        '15': '3.75rem',
      },
      height: {
        'half': '50vh',
        'halfw': '50vw',
      },
      maxHeight: {
        'halfw': '50vw',
      },
      minHeight: {
        '40': '10rem',
        '60': '15rem',
      },
      zIndex: {
        'bottom': '-999',
        'top': '999',
        '5': '5',
        '100': '100',
        '104': '104',
        'nav': '105',
        'burger': '110',
      },
      width: {
        '60': '15rem',
        '80': '20rem',
        '80p': '80%',
        '1320': '1320px',
      },
      minWidth: {
        '80': '20rem',
        '100': '25rem',
        '120': '30rem',
      },
    },
    screens: {
      'xs': '400px',
      'sm': '640px',
      'md': '768px',
      'lg': '820px',
      'hfd': '950px',
      '2lg': '1080px',
      'xl': '1200px',
      '2xl': '1500px',
      '3xl': '1700px',
      '4xl': '1920px',
    },
    fontSize: {
      'sm': '0.875rem',
      'md': '1.125rem',
      'base': '1.125rem',
      'lg': '1.625rem',
      'xl': '1.875rem',
      'mobile-subtitle': '2.125rem',
      'subtitle': '2.825rem',
      '2xl': '2.5rem',
      '3xl': '3.125rem',
      '4xl': '3.625rem',
      'title': '3.625rem',
      'btn': '4rem',
      'small-title': '3.5rem',
      'mobile-title': '5.375rem',
      'page-title': '7.125rem',
    },
  }
}
